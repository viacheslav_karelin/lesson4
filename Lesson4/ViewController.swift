//
//  ViewController.swift
//  Lesson4
//
//  Created by Admin on 05.10.17.
//  Copyright © 2017 inix. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    @IBAction func nextButton(_ sender: Any) {
        nextButtonPressed()
    }
    @IBAction func beforeButton(_ sender: Any) {
        beforeButtonPressed()
    }
    @IBAction func buttonStart(_ sender: UIButton) {
        startButtonPressed()
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initJob(jobIndex: currentJob)
        
    }
    
    
    var currentJob = 1      // Current Job
    var countOfAlljobs = 8  // Count of All jobs in hometask
    
    let jobDescription = [
        "Написати метод що виводить привітання “hello word”",
        "Написать метод который будет выводить приветствие передаваемое аргументом количество раз",
        "Написать метод в который передается номер дня(1-7),  и выводится строка с названием дня недели",
        "Сделать метод 3 не зависимым от номера дня (8- понедельник, 9 - вторник и т д)",
        "Числа Фибоначчи — линейная последовательность натуральных чисел, где первое и второе равно единице, а каждое последующее — сумме двух предыдущих.",
        "Дюймы в сантиметры 1 дюйм = 2.54см",
        "Сколько можно петь эту песню: \n 99 bottles of beer on the wall. \n 99 bottles of beer. \n You take one down, pass it around. 98 bottles of beer on the wall. \n 98 bottles of beer on the wall. . . .",
        "Написать программу которая выводит все числа которые можно нацело делить введенным числом в рамках 1…100"
    ]
    
    let initJobInputFieldValue = [-1, 0, 0, 0, 0, 0, 0, 0]
    
    /*
     1. написати метод що виводить привітання “hello word”
     */
    func task1() {
        printHello()
    }

    /*
     2. написать метод который будет выводить приветствие передаваемое аргументом количество раз
     */
    func task2() {
        let number = getIntegerFromInputField()
        if number <= 0 {
            return
        }
        for _ in 1...number {
            printHello()
        }
    }
    
    /*
     3. написать метод в который передается номер дня(1-7),  и выводится строка с названием дня недели
     */
    func task3() {
        let number = getIntegerFromInputField()
        if number <= 0 || number > 7 {
            print ("Error day off weak")
        }
        print("The \(number) day of weak is \(getDayOfWeak(dayOfWeak: number))")
    }
    
    /*
     4. сделать метод 3 не зависимым от номера дня (8- понедельник, 9 - вторник и т д)
     */
    func task4() {
        let number = getIntegerFromInputField()
        if number <= 0 {
            print ("Error day off weak")
        }
        print("The \(number) day of weak is \(getDayOfWeak(dayOfWeak: (number % 7)))")
    }
    
    /*
     5. Числа Фибоначчи — линейная последовательность натуральных чисел, где первое и второе равно единице, а каждое последующее — сумме двух предыдущих:
     F0 = 0 F1 = 1  F2 = 1 F3 = 2 F4 = 3 F5 = 5 F6 = 8
     (0 + 1) (1 + 1) (1 + 2)  (2 + 3) (3 + 5)
     Написать метод который выводит числа Фибоначчи до F(n)
     */
    func task5() {
        let number = getIntegerFromInputField()
        print ("F(0) = 0")
        if number == 0 {
            return
        }
        print ("F(1) = 1")
        if number == 1 {
            return
        }
        var F = 0
        var preValue = 0
        var currentValue = 1
        for i in 2...number {
            F = preValue + currentValue
            print("F(\(i)) = (\(preValue) + \(currentValue)) = \(F)")
            preValue = currentValue
            currentValue = F
        }
    }

    /*
     6. Дюймы в сантиметры 1 дюйм = 2.54см
     */
    func task6() {
        let number = getDoubleFromInputField()
        if number == -1 {
            return
        }
        print ("\(number) дюйм(ов) = \(Double(number) * 2.54)см")
    }

    /*
     7. Сколько можно петь эту песню: 99 bottles of beer on the wall. 99 bottles of beer. You take one down, pass it around. 98 bottles of beer on the wall.  98 bottles of beer on the wall. . . .
     */
    func task7() {
        let number = getIntegerFromInputField()
        if number == -1 {
            return
        }
        for i in 0..<number {
            print ("\(number - i) bottles of beer on the wall.")
            print ("\(number - i) bottles of beer.")
            print ("You take one down, pass it around. \(number - i - 1) bottles of beer on the wall.")
        }
    }

    /*
     8. Написать программу которая выводит все числа которые можно нацело делить введенным числом в рамках 1…100
     */
    func task8() {
        let number = getIntegerFromInputField()
        if number == -1 {
            return
        }
        for i in number...100 {
            if (i % number) == 0 {
                print (i)
            }
        }
    }

    func printHello() {
        print("Hello World")
    }
    
    func getDayOfWeak(dayOfWeak: Int) -> String{
        switch dayOfWeak {
        case 0:
            return "Неділя"
        case 1:
            return "Понеділок"
        case 2:
            return "Вівторок"
        case 3:
            return "Середа"
        case 4:
            return "Четвер"
        case 5:
            return "П'ятниця"
        case 6:
            return "Субота"
        case 7:
            return "Неділя"
        default:
            return "WTF?"
        }
    }
    
    func startButtonPressed(){
        print("Task: \(currentJob)")
        switch currentJob {
        case 1:
            task1()
        case 2:
            task2()
        case 3:
            task3()
        case 4:
            task4()
        case 5:
            task5()
        case 6:
            task6()
        case 7:
            task7()
        case 8:
            task8()
        default:
            return
        }
        initInputField(currentJob: currentJob)
    }
    
    func nextButtonPressed() {
        if currentJob + 1 <= countOfAlljobs {
            currentJob += 1
            initJob(jobIndex: currentJob)
        }
    }
    
    func beforeButtonPressed() {
        if currentJob - 1 > 0 {
            currentJob -= 1
            initJob(jobIndex: currentJob)
        }
    }
    
    func initJob(jobIndex: Int) {
        taskLabel.text = "Задание \(currentJob)"
        descriptionLabel.text = jobDescription[currentJob-1]
        setButtonTaskNumber(task: currentJob)
        initInputField(currentJob: currentJob)
    }
    
    func initInputField(currentJob: Int) {
        if initJobInputFieldValue[currentJob-1] == -1 {
            inputTextField.text = ""
            inputTextField.isEnabled = false
        }
        else
        {
            inputTextField.isEnabled = true
            inputTextField.text = ""
        }
    }
    
    // Show task number at button label
    func setButtonTaskNumber (task: Int) {
        let startLabel = "Start task # \(task)"
        startButton.setTitle(startLabel, for: .normal)
    }
    
    // Get integer number from input field, returning "-1" if it is not a number
    func getIntegerFromInputField() -> Int {
        if let inputText = (inputTextField.text) {
            if let myNumber = Int(inputText) {
                // print("number is \(myNumber)")
                return myNumber
            }
            else {
                print ("Input field does not have number")
            }
        }
        inputTextField.text = ""
        return -1
    }
    
    // Get integer number from input field, returning "-1" if it is not a number
    func getDoubleFromInputField() -> Double {
        if let inputText = (inputTextField.text) {
            if let myNumber = Double(inputText) {
                // print("number is \(myNumber)")
                return myNumber
            }
            else {
                print ("Input field does not have correct number")
            }
        }
        inputTextField.text = ""
        return -1
    }
}

